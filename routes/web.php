<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => 'auth'], function(){
    //to show list of films
    Route::get('/films', 'FilmCOntroller@index');

    //to show specfic film by slug name
    Route::get('/films/{film-slug-name}', 'FilmCOntroller@index');

    //to show a form for a new film
    Route::get('/films/create', 'FilmCOntroller@index');

    //to store film data
    Route::post('/films/store', 'FilmCOntroller@index');

});

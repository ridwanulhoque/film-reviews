<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class GenresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Genre::class, 10)->create();

    }
}

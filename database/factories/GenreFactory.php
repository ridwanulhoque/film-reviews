<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Genre;
use App\User;
use App\Film;
use Faker\Generator as Faker;

$factory->define(App\Genre::class, function(Faker $faker){
    return 
    [
     'created_by' => User::first()->id,
     'updated_by' => User::first()->id,
     'film_id' => Film::first()->id,
     'name' => $faker->name
    ]; 
 });

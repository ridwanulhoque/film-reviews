<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Film;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Film::class, function (Faker $faker) {
    return [
        'updated_by' => User::first()->id,
        'created_by' => User::first()->id,
        'slug' => $faker->unique()->name,
        'name' => $faker->unique()->name,
        'description' => $faker->paragraph,
        'release' => $faker->sentence,
        'release_date' => $faker->date,
        'rating' => $faker->randomNumber(1),
        'ticket' => $faker->sentence,
        'price' => $faker->randomNumber(3),
        'country' => $faker->country,
        'photo' => $faker->word.'.jpg'
    ];
});
